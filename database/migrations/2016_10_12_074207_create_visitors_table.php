<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('visitors', function (Blueprint $table) {
          $table->increments('id');
          $table->string('ip');
          $table->string('country');
          $table->string('regionName');
          $table->string('city');
          $table->string('lat');
          $table->string('lon');
          $table->string('isp');
          $table->string('ua_type');
          $table->string('os_name');
          $table->string('os_version');
          $table->string('browser_name');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('visitors');
    }
}
