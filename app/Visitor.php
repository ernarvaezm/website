<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
  protected $table = 'visitors';

  protected $fillable = [
      'country', 'regionName', 'city','lat','lon','isp','ip','ua_type','os_name'
      ,'os_version','browser_name'
  ];
}
