<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Storage;
use App\Http\Requests;
use App\Visitor;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use GuzzleHttp\Client;

class homeController extends Controller
{
protected $visitor;

public function __construct(Visitor $visitor  ){
 $this->visitor = $visitor;
}


public function downloadResume()
{
      return View('resume');
}

public function index()
{

$json = json_decode(file_get_contents('http://useragentapi.com/api/v4/json/461bcdd2/Mozilla%2F5.0+%28Windows+NT+6.3%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F57.0.2987.133+Safari%2F537.36'), true);

$ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ip));
if($query && $query['status'] == 'success') {
  $visitor = $this->visitor;
  $visitor->ip =$ip ;
  $visitor->country =$query['country'];
  $visitor->regionName =$query['regionName'];
  $visitor->city =$query['city'];
  $visitor->lat =$query['lat'];
  $visitor->lon =$query['lon'];
  $visitor->isp =$query['isp'];
  $visitor->ua_type =$json['data']['ua_type'];
  $visitor->os_name =$json['data']['os_name'];
  $visitor->os_version =$json['data']['os_version'];
  $visitor->browser_name =$json['data']['browser_name'];
  $visitor->save();
}
return View('welcome');
}

}
