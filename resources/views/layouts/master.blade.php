<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Eliecer Narvaez</title>
<link rel="shortcut icon" href="/assets/img/favicon.ico" type="favicon/ico" />
<!-- CSS -->
<link rel="stylesheet" href="/assets/css/owl.carousel.css">
<link rel="stylesheet" href="/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/assets/css/font-awesome.min.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/css/ionicons.min.css">
<link rel="stylesheet" href="/assets/css/animate.css">
<link rel="stylesheet" href="/assets/css/responsive.css">

</head>

<body>
  <!-- Header Start -->
  <header>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <!-- header Nav Start -->
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              </button>
          <a href="/">Eliecer Narvaez</a>
            </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                  <li><a href="/">Home</a></li>
                  <li><a href="/projects">Projects</a></li>
                  <li><a href="/getResume">Resume</a></li>

                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
      </div>
    </div>
  </header><!-- header close -->
      @yield('content')

      <!-- footer Start -->
      <footer>
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                    <div class="footer-manu">
                      <h1>Let's socialize</h1>
                          <br>
                        <div style="display:inline-block;width:100px">
                          <div class="clients-logo-img">
                          <a href="https://www.facebook.com/"><img src="/assets/img/social/fb.png" alt="Features"></a>
                          </div>
                        </div>

                        <div style="display:inline-block;width:100px">
                          <div class="clients-logo-img">
                                <a href="https://www.twitter.com/"><img src="/assets/img/social/tw.png" alt="Features"></a>
                          </div>
                        </div>
                        <div style="display:inline-block;width:100px">
                          <div class="clients-logo-img">
                              <a href="https://bitbucket.org/ernarvaezm/"><img src="/assets/img/social/bit.png" alt="Features"></a>
                          </div>
                        </div>
                        <div style="display:inline-block;width:100px">
                          <div class="clients-logo-img">
                            <a href="https://www.github.com/ernarvaezm"><img src="/assets/img/social/git.png" alt="Features"></a>
                          </div>
                        </div>
                        <div style="display:inline-block;width:100px">
                          <div class="clients-logo-img">
                            <a href="https://www.linkedin.com/in/ernarvaezm"><img src="/assets/img/social/li.png" alt="Features"></a>
                          </div>
                        </div>
                    </div>
                    <p>Copyright &copy; 2016</p>
                  </div>
              </div>
          </div>
      </footer>

      <!-- Js -->
      <script
        src="http://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="
        sha256-k2WSCIexGzOj3Euiig+TlR8gA0EmPjuc79OEeY5L45g="
        crossorigin="anonymous"></script>
      <script src="/assets/js/vendor/modernizr-2.6.2.min.js"></script>
      <script>window.jQuery || document.write('<script src="/assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script src="/assets/js/bootstrap.min.js"></script>
      <script src="/assets/js/owl.carousel.min.js"></script>
      <script src="/assets/js/plugins.js"></script>
      <script src="/assets/js/min/waypoints.min.js"></script>
      <script src="/assets/js/jquery.counterup.js"></script>
      <script src="/assets/js/main.js"></script>
      <script src="/assets/js/location.js"></script>
</body>
</html>
