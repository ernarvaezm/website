@extends('layouts.master')
@section('content')
    <!-- Slider Start -->
<section id="global-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="block">
                    <h1>Just some of my latest projects.</h1>
                    <p> Check out some of our latest work.</p>
                    <div id="clients-logo" class="owl-carousel">
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/php.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/rails.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/js.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/java.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/c.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/laravel.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/code.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/html.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/css.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/bit.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/github.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/perl.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/cisco.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/jquery.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/mysql.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/windows.png" alt="Features">
                      </div>
                      <div class="clients-logo-img">
                        <img src="/assets/img/clients/debian.png" alt="Features">
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
    <!-- Portfolio Start -->
<section id="portfolio-work">
    <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="block">
              <div class="portfolio-menu">
                <ul>
                    <li class="filter" data-filter="all">Everything</li>
                    <li class="filter" data-filter=".Desktop">Desktop</li>
                    <li class="filter" data-filter=".Websites">Web</li>
                    <li class="filter" data-filter=".Apps">Apps</li>
                </ul>
              </div>
              <div class="portfolio-contant">
                <ul id="portfolio-contant-active">
                  <li class="mix Websites">
                    <a href="http://campobusiness.com/">
                      <img src="/assets/img/portfolio/work7.png" alt="">
                      <!-- <div class="overly">
                        <div class="position-center">
                          <h2>Campobusiness.com</h2>
                          <p>Labore et dolore magna aliqua. Ut enim ad </p>

                        </div>
                      </div> -->
                    </a>
                </li>
                <li class="mix Desktop">
                  <a href="https://bitbucket.org/ernarvaezm/proyectocajero">
                    <img src="/assets/img/portfolio/work8.png" alt="">
                    <!-- <div class="overly">
                      <div class="position-center">
                        <h2>ATM</h2>
                        <p>Labore et dolore magna aliqua. Ut enim ad </p>

                      </div>
                    </div> -->
                  </a>
              </li>


                </ul>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>
@endsection
