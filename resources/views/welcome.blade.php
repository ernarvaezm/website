
@extends('layouts.master')
@section('content')


<!-- Slider Start -->
<section id="slider">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-md-offset-2">
        <div class="block">
          <h1 class="animated fadeInUp">Eliecer Narvaez <br>Software engineer</h1>
          <p>
            Software Engineer, strong, leadership, research. With emphasis
             in the back-end. My other skills include research, fast learning,
              goal achievement focus, teamwork and self motivation.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Wrapper Start -->
<section id="intro">
  <div class="container">
    <div class="row">
      <div class="col-md-7 col-sm-12">
        <div class="block">
          <div class="section-title">
            <h2>About Me</h2>
          <p>
Hello, I was born in one the best place in my beautiful country, Upala. When I was a kid, I've always loved the computers so my dream was to be a computer engineer.
In 2013, I moved to San Carlos to study software engineering in the UTN, then I obtained a scholarship to study telematics in Cenfotec University. In the meantime, I was learning a lot of technologies.
          </p>
          <p>
            <table class="table table-striped">
<thead>
  <tr>
    <th>Year</th>
    <th>Carrer</th>
    <th>Degree</th>
    <th>University</th>
  </tr>
</thead>
<tbody>
  <tr>
    <th scope="row">2016</th>
    <td>Software Engineering</td>
    <td>Diplomaed</td>
    <td>Universidad Técnica Nacional</td>
  </tr>
  <tr>
    <th scope="row">2015</th>
    <td>Telematics</td>
    <td>Technical</td>
    <td>Universidad Cenfotec</td>
  </tr>
</tbody>
</table>
          </p>
          </div>

        </div>
      </div><!-- .col-md-7 close -->
      <div class="col-md-5 col-sm-12">
        <div class="block">
          <img src="/assets/img/wrapper-img.gif" alt="Img">
        </div>
      </div><!-- .col-md-5 close -->
    </div>
  </div>
</section>

<section id="feature">
<div class="container">
  <div class="row">
    <div class="col-md-6 col-md-offset-6">
      <h2>I BELIEVE IN THE DREAMS</h2>
<p>
The biggest difference I’ve noticed between successful people and unsuccessful people isn’t intelligence or opportunity or resources. It’s the belief that they can make their goals happen.
</p>
<p>
I believe in myself. This confidence has made the difference for me again and again. I didn’t need intelligence or opportunity or resources. Just a simple belief in myself.
    </div>
  </div>
</div>
</section>

@endsection
